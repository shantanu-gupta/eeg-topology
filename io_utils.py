""" io.py
    
    Read/write files
    Plotting
"""

import numpy as np
import matplotlib.pyplot as plt

def read_data(filepath):
    with open(filepath) as f:
        data = [float(s) for s in f.read().split() if len(s.strip()) > 0]
    return np.array(data)

def pad_to_length(data, N):
    return np.pad(data, (N//2 - len(data)//2,) * 2, mode='constant')

def plot_PL(ax, ph_pairs, plot_title):
    for pair in ph_pairs:
        low, high = pair
        mid = (low + high) / 2.0
        height = (high - low) / 2.0
        x = [low, mid, high]
        y = [0.0, height, 0.0]
        ax.plot(x, y)
    ax.set_title(plot_title)
