# params.py
# Parameters for EEG analysis

from tda import area_distance_PL, wasserstein_distance_PH, layerwise_L2_PL

analysis_parameters = {
    # EEG channels
    'channels'      : ['c3', 'c4', 'cz', 'p3', 'p4', 't3', 't4', 't5'],
    # original data
    'data_dir'      : '/home/shantanu/data/eeg-yuanwang/data-wang.2018.annals',
    # PL/permutation test etc., plots
    'plots_dir'     : './plots',
    # plot size
    'plot_size'     : (8, 12), # width by height
    # plot file format
    'plot_format'   : 'eps',
    # intermediate data
    'idata_dir'     : './idata',
    # total number of samples per channel
    # a nice round number
    'n_samples'     : 32768,
    # sampling frequency
    'sample_freq'   : 100.0,
    # Smoothing hyperparameter
    # For damping high-frequency components
    'lp_sigma'      : 0.01,
    # Thresholding hyperparameter
    'lp_k'          : 500, # how many parameters to keep
    # Thresholding method used after low-pass filtering
    # 'mad'     : median absolute deviation
    # 'none'    : self-explanatory
    # 'top-k'   : take the top k components
    'lp_threshold_method'   : 'mad',
    # Method to get two different persistence homology sets to the same number
    # of pairs
    # 'fill' to create new epsilon-width pairs
    # 'cut' to remove the smallest pairs from the larger set
    'PH_balance_method'     : 'fill',
    # Resolution along x-axis in persistence landscapes
    'PL_resolution'         : 1.0e-2,
    # Similar balance as above, but strictly for persistence landscapes
    'PL_balance_method'     : 'fill',
    # Distance measure used for comparing PH sets
    'PH_distance_fn'        : wasserstein_distance_PH,
    # Distance measure for PL now
    'PL_distance_fn'        : layerwise_L2_PL,
    # How many times to resample the frequency components to perform the
    # permutation test on, later..
    'n_freq_resamples'      : 1000,
}
