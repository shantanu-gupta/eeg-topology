""" tda.py
    
    topological data analysis
    Compute persistent homology features from 1D signals.
"""

import numpy as np
from scipy.signal import argrelmin, argrelmax
from scipy.stats import rankdata, wasserstein_distance
import warnings

def _sort_by_nth_column(array, n):
    return array[array[:,n].argsort()]

def _get_critical_points_faster(signal):
    N = signal.shape[0]
    minima, maxima = [], []

    # The actual index of the critical points is not what we need, we need their
    # relative ordering alone.
    # So the 3rd critical point should have x=3. (and so on)
    rank = 0

    # Consider end points of the signal specially
    # Only minima change the number of components (add one)
    # Maxima don't merge components, as the other side does not exist at the
    # ends.
    if signal[0] <= signal[1]:
        minima.append([rank, signal[0]])
        rank += 1

    # Handle middle elements
    for i in range(1, N-1):
        list_to_add_to = None
        if signal[i] < signal[i-1] and signal[i] < signal[i+1]:
            list_to_add_to = minima
        elif signal[i] > signal[i-1] and signal[i] > signal[i+1]:
            list_to_add_to = maxima
        if list_to_add_to is not None:
            list_to_add_to.append([rank, signal[i]])
            rank += 1

    # Other end point of the signal
    if signal[-1] <= signal[-2]:
        minima.append([rank, signal[-1]])
        rank += 1

    return (_sort_by_nth_column(np.array(minima), 1),
            _sort_by_nth_column(np.array(maxima), 1))

def _get_critical_points(signal):
    idx_minima = argrelmin(signal)[0]
    idx_maxima = argrelmax(signal)[0]

    # Also consider the end points of the signal
    # But only minima change the number of components (add one)
    # Maxima don't merge components, as they only see one component approaching
    # them, from one side
    if signal[0] <= signal[1]:
        idx_minima = np.insert(idx_minima, 0, 0)
    if signal[-1] <= signal[-2]:
        idx_minima = np.append(idx_minima, len(signal)-1)

    # The actual index of the critical points is not what we need, we need their
    # relative ordering alone.
    # So the 3rd critical point should have x=3. (and so on)
    idx_critical = np.concatenate((idx_minima, idx_maxima))
    ranks = rankdata(idx_critical)
    ranks_minima = ranks[:idx_minima.shape[0]]
    ranks_maxima = ranks[idx_minima.shape[0]:]
    # Want nx2 arrays for minima and maxima
    minima = np.vstack((ranks_minima, signal[idx_minima])).T
    maxima = np.vstack((ranks_maxima, signal[idx_maxima])).T

    # Sort minima and maxima by y-value, not x.
    minima = _sort_by_nth_column(minima, 1)
    maxima = _sort_by_nth_column(maxima, 1)
    return minima, maxima

def _balance_ph_pairs(ph1, ph2, method='fill'):
    # For comparing sets of pairs, we need pairs of pairs.
    # Need to get both PHs to the same length somehow.
    if method == 'fill':
        PH_LENGTH_EPSILON = 1.0e-8
        while len(ph1) < len(ph2):
            ph1.append((0, PH_LENGTH_EPSILON))
        while len(ph2) < len(ph1):
            ph2.append((0, PH_LENGTH_EPSILON))
    elif method == 'cut':
        bar_length_fn = lambda bar: bar[1] - bar[0]
        sorted_ph1 = sorted(ph1, key=bar_length_fn)
        sorted_ph2 = sorted(ph2, key=bar_length_fn)
        n1, n2 = len(ph1), len(ph2)
        if n1 < n2:
            to_remove = sorted_ph2[:-n1]
            for e in to_remove:
                ph2.remove(e)
        elif n2 < n1:
            to_remove = sorted_ph1[:-n2]
            for e in to_remove:
                ph1.remove(e)
    else:
        raise NotImplementedError
    return

# TODO: make in-place
def _balance_pl(pl1, pl2, method='fill'):
    assert pl1.shape[1] == pl2.shape[1], print(pl1.shape, pl2.shape)
    N1, N2 = pl1.shape[0], pl2.shape[0]
    Ny = pl1.shape[1]
    if method == 'fill':
        to_fill = np.zeros((abs(N1 - N2), Ny))
        if N1 < N2:
            pl1 = np.vstack((to_fill, pl1))
        elif N2 < N1:
            pl2 = np.vstack((to_fill, pl2))
    elif method == 'cut':
        D = abs(N1 - N2)
        if N1 < N2:
            pl2 = pl2[D:,:]
        elif N2 < N1:
            pl1 = pl1[D:,:]
    else:
        raise NotImplementedError
    return pl1, pl2

def get_PH_pairs(signal):
    minima, maxima = _get_critical_points_faster(signal)

    i_minima, i_maxima = 0, 0
    N_min, N_max = minima.shape[0], maxima.shape[0]

    pairs = []
    active_minima = []
    x_covered = [False for i in range(N_min + N_max)]
    while i_minima < N_min or i_maxima < N_max:
        x_min, y_min = np.inf, np.inf
        x_max, y_max = np.inf, np.inf
        if i_minima < N_min:
            current_minimum = minima[i_minima,:]
            x_min, y_min = current_minimum
        if i_maxima < N_max:
            current_maximum = maxima[i_maxima,:]
            x_max, y_max = current_maximum

        if i_maxima == N_max or y_min <= y_max:
            active_minima.append(current_minimum)
            i_minima += 1
        elif i_minima == N_min or y_max < y_min:
            # Pair up with existing minima
            # The one to pair has to be adjacent (i.e., everything between it
            # and the max has to have been taken care of already)
            # Elder rule says that the pairing minimum will be the most recently
            # added, so we should go over the active minima backwards
            i_to_remove = None
            for i in range(len(active_minima)-1, -1, -1):
                x_min, y_min = active_minima[i]
                lower_x, higher_x = map(int, sorted([x_min, x_max]))
                if all(x_covered[lower_x+1:higher_x]):
                    # We got it!
                    pairs.append((y_min, y_max))
                    x_covered[lower_x], x_covered[higher_x] = True, True
                    i_to_remove = i
                    break
            if i_to_remove is not None:
                active_minima.pop(i_to_remove)
            else:
                warnings.warn('No matching minimum for '
                            + str(current_maximum))
            i_maxima += 1
    return pairs

def get_PL(ph_pairs, dy=1.0e-2):
    N = len(ph_pairs)
    y_min = min([p[0] for p in ph_pairs])
    y_max = max([p[1] for p in ph_pairs])

    Y_MIN, Y_MAX = -1.0, 1.0
    assert abs(y_min) <= 1 and abs(y_max) <= 1, print(y_min, y_max)

    Ny = int((Y_MAX - Y_MIN) // dy) + 1
    # First get the individual triangles
    PL = np.empty((N, Ny))
    t = np.linspace(Y_MIN, Y_MAX, num=Ny)
    for i, (a, b) in enumerate(ph_pairs):
        PL[i,:] = np.maximum(0, np.minimum(t - a, b - t))
    # Now sort them into layers
    PL.sort(axis=0)
    return PL
     
def layerwise_L2_PL(pl1, pl2, pl_balance_method='fill'):
    pl1, pl2 = _balance_pl(pl1, pl2, method=pl_balance_method)
    return np.linalg.norm(pl1 - pl2)

def area_distance_PL(ph1, ph2, ph_balance_method='fill'):
    # works on persistence landscapes created from the persistent homology pairs
    # in ph1 and ph2
    def triangle_difference_upto_intersection(g1, g2):
        lb1, ub1 = g1 # left
        lb2, ub2 = g2 # right

        # intersection point is mid-point of lb2 and ub1
        inter_x = (lb2 + ub1) / 2.0

        a1 = (0.25 * ((ub1 - lb1) ** 2)) - (0.5 * (ub1 - inter_x) ** 2)
        a2 = 0.5 * ((inter_x - lb2) ** 2)

        return a1 - a2

    def triangle_difference(g1, g2):
        # Subtracting areas of isosceles triangles angled at 45 degrees
        lb1, ub1 = g1
        lb2, ub2 = g2
        if lb2 < lb1:
            lb1, ub1, lb2, ub2 = lb2, ub2, lb1, ub1
        g1, g2 = (lb1, ub1), (lb2, ub2)

        if lb2 >= ub1:
            # no overlap
            return 0.25 * (((ub1 - lb1) ** 2)
                            + ((ub2 - lb2) ** 2))
        elif ub2 <= ub1:
            # full overlap
            return 0.25 * (((ub1 - lb1) ** 2)
                            + ((ub2 - lb2) ** 2))
        else:
            # partial overlap
            # divide total area into 2 parts
            A1 = triangle_difference_upto_intersection(g1, g2)
            
            gp1, gp2 = (-ub2, -lb2), (-ub1, -lb1)
            A2 = triangle_difference_upto_intersection(gp1, gp2)

            return A1 + A2

    _ph1, _ph2 = list(ph1), list(ph2)
    _balance_ph_pairs(_ph1, _ph2, method=ph_balance_method)
    bar_length_fn = lambda bar: bar[1] - bar[0]
    sorted_ph1 = sorted(_ph1, key=bar_length_fn)
    sorted_ph2 = sorted(_ph2, key=bar_length_fn)
    sum_areas = sum([triangle_difference(*pair)
                     for pair in zip(sorted_ph1, sorted_ph2)])
    return np.sqrt(sum_areas / min(len(ph1), len(ph2)))

def wasserstein_distance_PH(ph1, ph2, ph_balance_method='fill'):
    _ph1, _ph2 = list(ph1), list(ph2)
    _balance_ph_pairs(_ph1, _ph2, method=ph_balance_method)
    values_1, weights_1 = map(np.array, zip(*_ph1))
    weights_1 -= values_1
    values_2, weights_2 = map(np.array, zip(*_ph2))
    weights_2 -= values_2
    return wasserstein_distance(values_1, values_2, weights_1, weights_2)
