# eeg-topology
Topological data analysis of EEG signals.

Data:
	- 8 channels
	- Around 32K samples at 100 Hz
	- Half the samples are before a seizure event, half during
