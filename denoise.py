""" denoise.py
    
    Denoising 1D signals
"""

import numpy as np
from scipy.fftpack import dct, idct

def standardized(signal):
    s_01 = (signal - np.min(signal)) / (np.max(signal) - np.min(signal))
    return 2 * s_01 - 1 # want range to be [-1, 1]

def filter_lowpass(coeffs, sigma, k=None):
    N = coeffs.size
    lp_coeffs = np.copy(coeffs)
    if k is not None:
        lp_coeffs[k+1:] = 0
    window = np.arange(N) / float(N)
    window = np.exp(-(window ** 2) / (sigma ** 2))
    return coeffs * window

def threshold(signal, t):
    result = np.copy(signal)
    result[np.abs(result) < t] = 0
    return result

def mad_threshold(signal):
    N = signal.shape[0]
    med = np.median(np.abs(signal))
    mad = np.median(np.abs(signal - med))
    t = mad * np.sqrt(2 * np.log(N))
    return threshold(signal, t)

def top_k_threshold(signal, k):
    t = sorted(np.abs(signal))[-k]
    return threshold(signal, t)

def dct_denoise(signal, threshold_method='mad', k=None, lp_sigma=0.1):
    dct_coeffs = dct(signal, norm='ortho')
    dct_coeffs = filter_lowpass(dct_coeffs, lp_sigma)
    if threshold_method == 'mad':
        clean_coeffs = mad_threshold(dct_coeffs)
    elif threshold_method == 'top-k':
        clean_coeffs = top_k_threshold(dct_coeffs, k)
    elif threshold_method == 'none':
        clean_coeffs = dct_coeffs
    return idct(dct_coeffs, norm='ortho')

def fft_denoise(signal, threshold_method='mad', k=None, lp_sigma=0.1):
    fft_coeffs = np.fft.rfft(signal)
    fft_coeffs = filter_lowpass(fft_coeffs, lp_sigma, k=k)
    
    if threshold_method == 'none':
        clean_coeffs = fft_coeffs
    elif threshold_method == 'mad':
        clean_coeffs = mad_threshold(fft_coeffs)
    elif threshold_method == 'top-k':
        clean_coeffs = top_k_threshold(fft_coeffs, k)
    else:
        raise NotImplementedError

    return np.fft.irfft(clean_coeffs)

def get_frequency_permutation(s1, s2):
    S1, S2 = np.fft.rfft(s1), np.fft.rfft(s2)
    flipped = np.random.RandomState().choice([0, 1], size=S1.shape)

    to_flip = (flipped == 1)
    S1[to_flip], S2[to_flip] = S2[to_flip], S1[to_flip]
    return (flipped, standardized(np.fft.irfft(S1)),
                     standardized(np.fft.irfft(S2)))
