#!/usr/bin/env python

import os
import warnings
from pprint import pprint
import concurrent.futures
import datetime
import pickle

import numpy as np
from scipy.fftpack import dct, idct
from scipy.stats import rankdata
import matplotlib.pyplot as plt

from denoise import dct_denoise, fft_denoise
from denoise import standardized, get_frequency_permutation
from tda import get_PH_pairs, get_PL, area_distance_PL, wasserstein_distance_PH
from io_utils import read_data, pad_to_length, plot_PL

from params import analysis_parameters
DATA_DIR    = analysis_parameters['data_dir']
IDATA_DIR   = analysis_parameters['idata_dir']
PLOTS_DIR   = analysis_parameters['plots_dir']
PLOT_SIZE   = analysis_parameters['plot_size']
PLOT_FORMAT = analysis_parameters['plot_format']
SAMPLE_FREQ = analysis_parameters['sample_freq']
N           = analysis_parameters['n_samples']
CHANNELS    = analysis_parameters['channels']
N_CHANNELS  = len(CHANNELS)

LP_THRESHOLD_METHOD = analysis_parameters['lp_threshold_method']
LP_SIGMA            = analysis_parameters['lp_sigma']
LP_K                = analysis_parameters['lp_k']
PH_BALANCE_METHOD   = analysis_parameters['PH_balance_method']
PH_DISTANCE_FN      = analysis_parameters['PH_distance_fn']
PL_RESOLUTION       = analysis_parameters['PL_resolution']
PL_BALANCE_METHOD   = analysis_parameters['PL_balance_method']
PL_DISTANCE_FN      = analysis_parameters['PL_distance_fn']
N_RESAMPLES         = analysis_parameters['n_freq_resamples']

TIMESTAMP = str(datetime.datetime.now())
analysis_parameters['timestamp'] = TIMESTAMP
IDATA_DIR = os.path.join(IDATA_DIR, str(TIMESTAMP))
PLOTS_DIR = os.path.join(PLOTS_DIR, str(TIMESTAMP))
if not os.path.exists(IDATA_DIR):
    os.makedirs(IDATA_DIR)
if not os.path.exists(PLOTS_DIR):
    os.makedirs(PLOTS_DIR)
print('Analysis parameters:')
pprint(analysis_parameters)

# Saving parameters
print('Saving experiment parameters...')
with open(os.path.join(IDATA_DIR, 'params.pickle'), 'wb') as pfile:
    pickle.dump(analysis_parameters, pfile)

# Re-sample in frequency domain
def resample(before, during):
    idx, r_before, r_during = get_frequency_permutation(before, during)
    ph_before, ph_during = get_PH_pairs(r_before), get_PH_pairs(r_during)
    return (idx, r_before, r_during, ph_before, ph_during,
            PH_DISTANCE_FN(ph_before, ph_during,
                            ph_balance_method=PH_BALANCE_METHOD))
    # return (idx, r_before, r_during, ph_before, ph_during,
    #         PL_DISTANCE_FN(get_PL(ph_before, dy=PL_RESOLUTION),
    #                         get_PL(ph_during, dy=PL_RESOLUTION),
    #                         pl_balance_method=PL_BALANCE_METHOD))

original_all_channels = {} # will store the original data from all channels
denoised_all_channels = {} # will store denoised signals from all channels
ph_pairs_all_channels = {} # will store PH pairs from all channels together 

# read data
for c in CHANNELS:
    print('=' * 80)
    print('Working on channel {}...'.format(c))
    eeg = {}
    print('Reading data...')
    fpath = os.path.join(DATA_DIR, c)
    data = pad_to_length(read_data(fpath), N)
    # Mean-centre the data -- probably needed for DCT
    eeg['mean'] = data.mean()
    eeg['full_centred'] = data - eeg['mean']
    eeg['original'] = {'before': eeg['full_centred'][:N//2],
                       'during': eeg['full_centred'][N//2:]}

    original_all_channels[c] = eeg['original']

    # denoise data by throwing away coefficients under a certain threshold
    # then extract persistent homology features
    print('Denoising data and extracting persistent homology pairs...')
    # Work on the whole data first.
    denoised = fft_denoise(eeg['full_centred'],
                            threshold_method='none',
                            k=LP_K, lp_sigma=LP_SIGMA)

    # Get the 2 phases of the signal separated
    before = denoised[:N//2]
    during = denoised[N//2:]

    # Denoise each phase
    denoised_before = fft_denoise(before,
                                    threshold_method='mad',
                                    k=LP_K, lp_sigma=1.0e6) # no LP this time
    denoised_during = fft_denoise(during,
                                    threshold_method='mad',
                                    k=LP_K, lp_sigma=1.0e6)

    # Scale both phases down to equivalent amplitude.
    denoised_before = standardized(denoised_before)
    denoised_during = standardized(denoised_during)

    eeg['denoised'] = {'before': denoised_before,
                       'during': denoised_during}

    # Get persistence landscape from signal
    eeg['ph_pairs'] = {'before': get_PH_pairs(denoised_before),
                       'during': get_PH_pairs(denoised_during)}

    denoised_all_channels[c] = eeg['denoised']
    ph_pairs_all_channels[c] = eeg['ph_pairs']

    # Permutation test
    print('Beginning permutation test...')
    checkpoints = [i * (N_RESAMPLES // 100) for i in range(1, 101)]
    if checkpoints[-1] != N_RESAMPLES:
        checkpoints.append(N_RESAMPLES)

    # eeg['d0'] = PL_DISTANCE_FN(get_PL(eeg['ph_pairs']['before'],
    #                                 dy=PL_RESOLUTION),
    #                            get_PL(eeg['ph_pairs']['during'],
    #                                 dy=PL_RESOLUTION),
    #                            pl_balance_method=PL_BALANCE_METHOD)
    eeg['d0'] = PH_DISTANCE_FN(eeg['ph_pairs']['before'],
                               eeg['ph_pairs']['during'],
                               ph_balance_method=PH_BALANCE_METHOD)
    eeg['resamples'] = []
    eeg['resample_ph'] = []
    eeg['resample_dists'] = []
    eeg['resample_idxs'] = [] # indexes swapped in permutation test

    before = [eeg['denoised']['before']] * N_RESAMPLES # TODO:find better way
    during = [eeg['denoised']['during']] * N_RESAMPLES

    with concurrent.futures.ProcessPoolExecutor() as executor:
        for i, result in enumerate(executor.map(resample, before, during,
                                            chunksize=max(1,N_RESAMPLES//100))):
            i += 1
            idxs, r_before, r_during, r_ph_before, r_ph_during, r_dist = result
            eeg['resample_idxs'].append(idxs)
            eeg['resamples'].append({'before': r_before, 'during': r_during})
            eeg['resample_ph'].append({'before': r_ph_before,
                                       'during': r_ph_during})
            eeg['resample_dists'].append(r_dist)
            if i in checkpoints:
                print('{} - {} {}/{} ({:.2f}%)'.format(datetime.datetime.now(),
                                                        c, i, N_RESAMPLES,
                                                        (i * 100)/N_RESAMPLES))

    d0 = eeg['d0']
    dists = eeg['resample_dists']
    print('d0 for {}: {}'.format(c, d0))
    print('{}: p(dist > d0) = {}'.format(c,
                                    (np.array(dists) > d0).sum() 
                                        / len(dists)))
    
    plt.figure()
    plt.title('{} distance histogram from permutation test'.format(c))
    plt.hist(dists, bins=100)
    plt.axvline(x=d0, color='black')
    plt.savefig(os.path.join(PLOTS_DIR, '{}_pdist.{}'.format(c, PLOT_FORMAT)))

    print('Saving data...')
    with open(os.path.join(IDATA_DIR, '{}.pickle'.format(c)), 'wb') as idata:
        pickle.dump(eeg, idata)
    print('Done!')

# Plotting original signal
print('Plotting the original signals...')
fig, axes = plt.subplots(N_CHANNELS, 2, sharex=True)
fig.set_size_inches(*PLOT_SIZE)
fig.suptitle('Original signal (mean-centred)')
for c, (ax_before, ax_during) in zip(CHANNELS, axes):
    ax_before.plot(original_all_channels[c]['before'])
    ax_before.set_title('{}_before'.format(c))

    ax_during.plot(original_all_channels[c]['during'])
    ax_during.set_title('{}_during'.format(c))
fig.subplots_adjust(hspace=0.6)
fig.savefig(os.path.join(PLOTS_DIR, 'original.{}'.format(PLOT_FORMAT)))

# Plot denoised signal and persistence landscapes
print('Plotting the denoised signals and their persistence landscapes...')
# Denoised
fig, axes = plt.subplots(N_CHANNELS, 2, sharex=True)
fig.set_size_inches(*PLOT_SIZE)
fig.suptitle('Denoised signal (scale-normalized)')
for c, (ax_before, ax_during) in zip(CHANNELS, axes):
    ax_before.plot(denoised_all_channels[c]['before'])
    ax_before.set_title('{}_before'.format(c))

    ax_during.plot(denoised_all_channels[c]['during'])
    ax_during.set_title('{}_during'.format(c))
fig.subplots_adjust(hspace=0.6)
fig.savefig(os.path.join(PLOTS_DIR, 'denoised.{}'.format(PLOT_FORMAT)))
# PL
fig, axes = plt.subplots(N_CHANNELS, 2, sharex=True, sharey=True)
fig.set_size_inches(*PLOT_SIZE)
fig.suptitle('Persistence Landscapes')
for c, (ax_before, ax_during) in zip(CHANNELS, axes):
    pairs_before = ph_pairs_all_channels[c]['before']
    pairs_during = ph_pairs_all_channels[c]['during']

    for pair_set, ax, plot_name in zip([pairs_before, pairs_during],
                                       [ax_before, ax_during],
                                       ['PL_before', 'PL_during']):
        plot_PL(ax, pair_set, '{}_{}'.format(c, plot_name))
fig.subplots_adjust(hspace=0.6)
fig.savefig(os.path.join(PLOTS_DIR, 'PL.{}'.format(PLOT_FORMAT)))

print('All done!')
